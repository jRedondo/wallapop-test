# Wallapop

This project was made as a technical test for Wallapop.
This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 9.0.4.<br />
The async request for products is being "faked" using [in-memory-web-api](https://github.com/angular/in-memory-web-api) as I was getting a cross origin error. Tried to fix it but wasn't able to succeed.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).<br />
It may be missing some test as I'm still learning how to do unit testing.
