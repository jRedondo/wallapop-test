import { Injectable } from '@angular/core';
import { ProductInterface } from '../models/product';
import { BehaviorSubject, Observable } from 'rxjs';
import { HttpClient} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class FavouritesService {
  private urlAPI = 'https://webpublic.s3-eu-west-1.amazonaws.com/tech-test/items.json';
  private itemsInFavouriteSubject: BehaviorSubject<ProductInterface[]> = new BehaviorSubject([]);
  private itemsInFavourite: ProductInterface[] = [];

  constructor(private http: HttpClient) {
    this.itemsInFavouriteSubject.subscribe(_ => this.itemsInFavourite = _);
  }

  public addToFavourites(product: ProductInterface) {
    this.itemsInFavouriteSubject.next([...this.itemsInFavourite, product]);
  }

  public removeFromFavourites(product: ProductInterface) {
    const currentFavourites = [...this.itemsInFavourite];
    const favouritesWithoutRemoved = currentFavourites.filter(favItem => favItem !== product);

    this.itemsInFavouriteSubject.next(favouritesWithoutRemoved);
  }

  public isFavourite(product: ProductInterface) {
    return this.itemsInFavourite.indexOf(product) >= 0;
  }

  public getItems(): Observable<ProductInterface[]> {
    return this.itemsInFavouriteSubject.asObservable();
  }

  public getAllProducts(): Observable<ProductInterface[]> {
    return this.http.get<ProductInterface[]>(this.urlAPI);
  }
}
