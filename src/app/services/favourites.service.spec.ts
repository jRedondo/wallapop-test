/* tslint:disable:no-unused-variable */

import {TestBed, async, inject} from '@angular/core/testing';
import {FavouritesService} from './favourites.service';
import {HttpClientTestingModule, HttpTestingController} from '@angular/common/http/testing';
import {ProductInterface} from '../models/product';

describe('Service: Favourites', () => {
  const mockItem: ProductInterface = {
    title: 'Test item',
    description: 'Test description',
    price: 999,
    email: 'testmail@test.com',
    image: 'https://webpublic.s3-eu-west-1.amazonaws.com/tech-test/img/iphone.png'
  };

  const mockResponse: ProductInterface[] = [mockItem];
  
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [FavouritesService]
    });
  });

  it('should get all products: getAllProducts',
    inject([HttpTestingController, FavouritesService], (httpMock: HttpTestingController, favouritesServiceTested: FavouritesService) => {

      const swapiUrl = 'https://webpublic.s3-eu-west-1.amazonaws.com/tech-test/items.json';

      favouritesServiceTested.getAllProducts()
        .subscribe(
          (res) => {
            expect(res).toEqual(mockResponse);
          }
        );
      const req = httpMock.expectOne(swapiUrl);
      expect(req.request.method).toBe('GET');
      req.flush(mockResponse);
    }));

  it('should favourite items be empty on start: getItems',
    inject([FavouritesService], (favouritesServiceTested: FavouritesService) => {

      favouritesServiceTested.getItems()
        .subscribe(
          (res) => {
            expect(res).toEqual([]);
          }
        );
    }));

  it('should add element to favourites: addToFavourites',
    inject([FavouritesService], (favouritesServiceTested: FavouritesService) => {
      favouritesServiceTested.addToFavourites(mockItem);
      favouritesServiceTested.getItems()
        .subscribe(
          (res) => {
            expect(res.length).toEqual(1);
          }
        );
    }));

  it('should delete element from favourites: removeFromFavourites',
    inject([FavouritesService], (favouritesServiceTested: FavouritesService) => {
      favouritesServiceTested.addToFavourites(mockItem);
      favouritesServiceTested.removeFromFavourites(mockItem);
      favouritesServiceTested.getItems()
        .subscribe(
          (res) => {
            expect(res.length).toEqual(0);
          }
        );
    }));

  it('should check if element is in favourites: isFavourite',
    inject([FavouritesService], (favouritesServiceTested: FavouritesService) => {
      const item: ProductInterface = {
        title: 'Test item',
        description: 'Test description',
        price: 999,
        email: 'testmail@test.com',
        image: 'https://webpublic.s3-eu-west-1.amazonaws.com/tech-test/img/iphone.png'
      };

      expect(favouritesServiceTested.isFavourite(item)).toBeFalse();
      favouritesServiceTested.addToFavourites(item);
      expect(favouritesServiceTested.isFavourite(item)).toBeTrue();
    }));

});
