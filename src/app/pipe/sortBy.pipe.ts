import { Pipe, PipeTransform } from '@angular/core';
import { orderBy } from 'lodash';
import {ProductInterface} from '../models/product';


@Pipe({
  name: 'sortBy',
})
export class SortByPipe implements PipeTransform {
  transform(value: ProductInterface[], order = '', column: string = ''): any[] {
    if (!value || order === '' || !order) { return value; } // no array
    if (value.length <= 1) { return value; } // array with only one item
    if (!column || column === '') {
      if (order === 'asc') {
        return value.sort();
      } else {
        return value.sort().reverse();
      }
    } // sort 1d array

    const valueMod = value.map(product => ({ ...product, price: +product.price}));
    return orderBy(valueMod, [column], [order]);
  }
}
