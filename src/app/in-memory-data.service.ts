import {InMemoryDbService} from 'angular-in-memory-web-api';
import {wallapopProducts} from './wallapopProducs';

export class InMemoryDataService implements InMemoryDbService {
  createDb(): {} {
    return wallapopProducts;
  }
}
