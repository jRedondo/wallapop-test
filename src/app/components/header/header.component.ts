import { Component, OnInit } from '@angular/core';
import { FavouritesService } from '../../services/favourites.service';
import { Observable } from 'rxjs';
import {ProductInterface} from '../../models/product';
import { ModalService } from '../_modal';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  public favouriteProducts: Observable<ProductInterface[]>;
  title: string = 'Wallapop';

  constructor(
    private modalService: ModalService,
    private favouriteService: FavouritesService
  ) {
    this.favouriteProducts = this.favouriteService.getItems();
    this.favouriteProducts.subscribe(_ => _);
  }

  ngOnInit(): void {

  }

  openModal(id: string) {
    this.modalService.open(id);
  }
}
