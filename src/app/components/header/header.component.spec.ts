import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {HeaderComponent} from './header.component';
import { HttpClientModule} from '@angular/common/http';

describe('Component: HeaderComponent', () => {

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientModule],
      declarations: [HeaderComponent]
    })
      .compileComponents();
  }));


  it(`should have as logo title 'Wallapop'`, () => {
    const fixture = TestBed.createComponent(HeaderComponent);
    const app = fixture.componentInstance;
    expect(app.title).toEqual('Wallapop');
  });

  it(`should open favourites modal on click: openModal'`, () => {
    const fixture = TestBed.createComponent(HeaderComponent);
    const comp = fixture.componentInstance;
    expect(comp.title).toEqual('Wallapop');
  });
});
