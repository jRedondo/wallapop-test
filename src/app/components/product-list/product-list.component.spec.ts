/* tslint:disable:no-unused-variable */
import {async, ComponentFixture, fakeAsync, inject, TestBed, tick} from '@angular/core/testing';
import {By} from '@angular/platform-browser';
import {DebugElement} from '@angular/core';

import {ProductListComponent} from './product-list.component';
import {Test} from 'tslint';
import {FavouritesService} from '../../services/favourites.service';
import {Observable} from 'rxjs';
import {ProductInterface} from '../../models/product';
import {ModalService} from '../_modal';
import {HttpClientTestingModule} from '@angular/common/http/testing';

describe('Component: Product List', () => {
  let component: ProductListComponent;
  let fixture: ComponentFixture<ProductListComponent>;

  const mockItemA: ProductInterface = {
    title: 'Test A',
    description: 'Test description A',
    price: 999,
    email: 'testmail@test.com',
    image: 'https://webpublic.s3-eu-west-1.amazonaws.com/tech-test/img/iphone.png'
  };

  const mockItemB: ProductInterface = {
    title: 'Test B',
    description: 'Test description B',
    price: 0,
    email: 'testmail@test.com',
    image: 'https://webpublic.s3-eu-west-1.amazonaws.com/tech-test/img/iphone.png'
  };

  const mockResponse: ProductInterface[] = [mockItemA, mockItemB];

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ProductListComponent],
      imports: [HttpClientTestingModule],
      providers: [FavouritesService, ModalService]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should display items in page, 5 per page: displayActivePage', () => {
    component.filteredProducts = mockResponse;
    component.displayActivePage(1);
    fixture.detectChanges();
    expect(component.paginatedProducts).toEqual(mockResponse);
    component.displayActivePage(2);
    fixture.detectChanges();
    expect(component.paginatedProducts).toEqual([]);
  });

  it('should sort items: sortProductList', () => {
    component.products = mockResponse;
    component.filterProductList('Test A');
    fixture.detectChanges();
    expect(component.filteredProducts).toEqual([mockItemA]);
  });

  it('should filter items: filterProductList', () => {
    component.filteredProducts = mockResponse;
    component.sortProductList('price');
    fixture.detectChanges();
    expect(component.filteredProducts).toEqual([mockItemB, mockItemA]);
  });
});
