import {ChangeDetectorRef, Component, OnInit, Input, OnChanges} from '@angular/core';
import {FavouritesService} from '../../services/favourites.service';
import {ProductInterface} from '../../models/product';
import {ModalService} from '../_modal';
import {Observable} from 'rxjs';
import {orderBy} from 'lodash';

@Component({
  selector: 'app-product-list',
  templateUrl: './product-list.component.html',
  styleUrls: ['./product-list.component.scss']
})
export class ProductListComponent implements OnInit {
  public favouriteProducts: Observable<ProductInterface[]>;
  @Input() filter: string;
  @Input() searchByKeyword: string;

  sortElement: string = 'title';
  products: ProductInterface[];
  filteredProducts: ProductInterface[] = [];
  paginatedProducts: ProductInterface[] = [];
  activePage: number = 0;
  isLoading: boolean = true;

  constructor(
    private modalService: ModalService,
    private favouriteService: FavouritesService,
    private ref: ChangeDetectorRef
  ) {
    this.favouriteProducts = this.favouriteService.getItems();
    this.favouriteProducts.subscribe(_ => _);
  }

  ngOnInit() {
    this.favouriteService.getAllProducts().subscribe(data => {
      const parsedProducts = data.map(product => ({...product, price: +product.price}));

      this.isLoading = false;
      this.products = parsedProducts;
      this.filteredProducts = parsedProducts;
      this.sortProductList(this.sortElement);
      this.displayActivePage(1);
    });
  }

  displayActivePage(activePageNumber: number) {
    this.activePage = activePageNumber;
    this.paginatedProducts = this.filteredProducts.slice(5 * (activePageNumber - 1), 5 * (activePageNumber - 1) + 5);
  }

  filterProductList(filter: string): void {
    this.filteredProducts = [...this.products];

    const regex = new RegExp(filter, 'i');

    this.filteredProducts = this.products.filter(product => {
      return regex.test(product.title) ||
        regex.test(product.description) ||
        regex.test(product.email) ||
        regex.test(product.price.toString());
    });

    this.sortProductList(this.sortElement);

    this.displayActivePage(1);
  }

  sortProductList(sort: string): void {
    this.sortElement = sort;
    const items = [...this.filteredProducts];

    if (sort === 'price') {
      this.filteredProducts = orderBy(items, [this.sortElement], 'asc');
    } else {
      this.filteredProducts = orderBy(items, [product => product[this.sortElement].toLowerCase()], 'asc');
    }

    this.displayActivePage(this.activePage);
  }
}
