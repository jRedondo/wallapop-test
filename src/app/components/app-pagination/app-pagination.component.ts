import {Component, Input, OnChanges, Output, EventEmitter} from '@angular/core';

@Component({
  selector: 'app-pagination',
  templateUrl: './app-pagination.component.html'
})

export class PaginationComponent implements OnChanges {
  @Input() totalRecords: number = 0;
  @Input() recordsPerPage: number = 0;
  @Input() activePage: number;

  @Output() onPageChange: EventEmitter<number> = new EventEmitter();

  public pages: number [] = [];

  ngOnChanges() {
    const pageCount = this.getPageCount();
    this.pages = this.getArrayOfPage(pageCount);
  }

  private getPageCount(): number {
    let totalPage: number = 0;

    if (this.totalRecords > 0 && this.recordsPerPage > 0) {
      const pageCount = this.totalRecords / this.recordsPerPage;
      const roundedPageCount = Math.floor(pageCount);

      totalPage = roundedPageCount < pageCount ? roundedPageCount + 1 : roundedPageCount;
    }

    return totalPage;
  }

  private getArrayOfPage(pageCount: number): number [] {
    const pageArray: number [] = [];

    if (pageCount > 0) {
      for (let i = 1; i <= pageCount; i++) {
        pageArray.push(i);
      }
    }

    return pageArray;
  }

  onClickPage(pageNumber: number) {
    if (pageNumber < 1) {
      return;
    }
    if (pageNumber > this.pages.length) {
      return;
    }

    this.onPageChange.emit(pageNumber);
  }
}
