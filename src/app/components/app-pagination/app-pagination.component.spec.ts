import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {PaginationComponent} from './app-pagination.component';

describe('Component: Pagination', () => {
  let component: PaginationComponent;
  let fixture: ComponentFixture<PaginationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [PaginationComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PaginationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create pagination', () => {
    expect(component).toBeTruthy();
  });

  it('should emit onPageChange on click Next button', () => {
    const fixture = TestBed.createComponent(PaginationComponent);
    const component = fixture.componentInstance;
    component.activePage = 1;
    component.pages = [1, 2];

    spyOn(component.onPageChange, 'emit');

    const nativeElement = fixture.nativeElement;
    const button = nativeElement.querySelector('.next');
    button.dispatchEvent(new Event('click'));

    fixture.detectChanges();

    expect(component.onPageChange.emit).toHaveBeenCalledWith(2);
  });

  it('should emit onPageChange on click Previous button', () => {
    const fixture = TestBed.createComponent(PaginationComponent);
    const component = fixture.componentInstance;
    component.activePage = 2;
    component.pages = [1, 2, 3];

    spyOn(component.onPageChange, 'emit');

    const nativeElement = fixture.nativeElement;
    const button = nativeElement.querySelector('.previous');
    button.dispatchEvent(new Event('click'));

    fixture.detectChanges();

    expect(component.onPageChange.emit).toHaveBeenCalledWith(1);
  });

  it('should not emit previous changePage when it\'s first one', () => {
    const fixture = TestBed.createComponent(PaginationComponent);
    const component = fixture.componentInstance;
    component.activePage = 1;
    component.pages = [1, 2];

    spyOn(component.onPageChange, 'emit');

    // trigger the click
    const nativeElement = fixture.nativeElement;
    const button = nativeElement.querySelector('.previous');
    button.dispatchEvent(new Event('click'));

    fixture.detectChanges();

    expect(component.onPageChange.emit).not.toHaveBeenCalled();
  });

  it('should not emit next changePage when it\'s last one', () => {
    const fixture = TestBed.createComponent(PaginationComponent);
    const component = fixture.componentInstance;
    component.activePage = 2;
    component.pages = [1, 2];

    spyOn(component.onPageChange, 'emit');

    // trigger the click
    const nativeElement = fixture.nativeElement;
    const button = nativeElement.querySelector('.next');
    button.dispatchEvent(new Event('click'));

    fixture.detectChanges();

    expect(component.onPageChange.emit).not.toHaveBeenCalled();
  });

});
