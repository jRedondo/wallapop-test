import { Component, OnInit } from '@angular/core';
import {FavouritesService} from '../../services/favourites.service';
import {Observable} from 'rxjs';
import {ProductInterface} from '../../models/product';

@Component({
  selector: 'app-favourite-list',
  templateUrl: './favourite-list.component.html',
  styleUrls: ['./favourite-list.component.scss']
})
export class FavouriteListComponent implements OnInit {
  public favouriteProducts: Observable<ProductInterface[]>;
  filteredProducts: ProductInterface[] = [];
  favouriteItems: ProductInterface[] = [];

  constructor(
    private favouriteService: FavouritesService,
  ) {
    this.favouriteProducts = this.favouriteService.getItems();
    this.favouriteProducts.subscribe(_ => {
      this.favouriteItems = _;
      this.filteredProducts = _;
      return _;
    });
  }

  filterFavList(filter: string): void {
    this.filteredProducts = [...this.favouriteItems];

    const regex = new RegExp(filter, 'i');

    this.filteredProducts = this.filteredProducts.filter(product => {
      return regex.test(product.title);
    });
  }

  ngOnInit(): void {
  }

}
