import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FavouriteListComponent } from './favourite-list.component';
import {ProductInterface} from '../../models/product';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {FavouritesService} from '../../services/favourites.service';

describe('Component: FavouriteList', () => {
  let component: FavouriteListComponent;
  let fixture: ComponentFixture<FavouriteListComponent>;

  const mockItemA: ProductInterface = {
    title: 'Test A',
    description: 'Test description A',
    price: 999,
    email: 'testmail@test.com',
    image: 'https://webpublic.s3-eu-west-1.amazonaws.com/tech-test/img/iphone.png'
  };

  const mockItemB: ProductInterface = {
    title: 'Test B',
    description: 'Test description B',
    price: 0,
    email: 'testmail@test.com',
    image: 'https://webpublic.s3-eu-west-1.amazonaws.com/tech-test/img/iphone.png'
  };

  const mockResponse = [mockItemA, mockItemB];

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FavouriteListComponent ],
      imports: [HttpClientTestingModule],
      providers: [FavouritesService]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FavouriteListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be empty on start: Start', () => {
    fixture.detectChanges();

    expect(component.filteredProducts).toEqual([]);
  });

  it('should filter items: filterFavList', () => {
    component.favouriteItems = mockResponse;
    component.filterFavList('Test B');
    fixture.detectChanges();

    expect(component.filteredProducts).toEqual([mockItemB]);
  });
});
