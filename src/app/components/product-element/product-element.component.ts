import {Component, OnInit, Input, OnChanges} from '@angular/core';
import {ProductInterface} from '../../models/product';
import { FavouritesService } from '../../services/favourites.service';
import {Observable} from 'rxjs';
import {CurrencyPipe} from '@angular/common';

@Component({
  selector: 'app-product-element',
  templateUrl: './product-element.component.html',
  styleUrls: ['./product-element.component.scss']
})
export class ProductElementComponent implements OnInit {
  @Input() product: ProductInterface;
  public favouriteProducts: Observable<ProductInterface[]>;
  public favouritesArray: ProductInterface[];
  isFavourite: boolean;

  onClickAddRemoveFavourites(product) {
    if (this.isFavourite) {
      this.favouriteService.removeFromFavourites(product);
      this.isFavourite = false;
    } else {
      this.favouriteService.addToFavourites(product);
      this.isFavourite = true;
    }
  }

  constructor(
    private favouriteService: FavouritesService
  ) {
    this.favouriteProducts = this.favouriteService.getItems();

    this.favouriteProducts.subscribe(_ => {
      this.isFavourite = this.favouriteService.isFavourite(this.product);
      return _;
    });
  }

  ngOnInit(): void {
    this.isFavourite = this.favouriteService.isFavourite(this.product);
  }

}
