import {async, ComponentFixture, fakeAsync, inject, TestBed} from '@angular/core/testing';

import {ProductElementComponent} from './product-element.component';
import {FavouritesService} from '../../services/favourites.service';
import {wallapopProducts} from '../../wallapopProducs';
import {HttpClientModule} from '@angular/common/http';
import {ProductInterface} from '../../models/product';
import {Test} from 'tslint';
import {DebugElement} from '@angular/core';
import {HttpClientTestingModule} from '@angular/common/http/testing';

import {CurrencyPipe, registerLocaleData} from '@angular/common';
import localeEs from '@angular/common/locales/es';


describe('Component: Product element', () => {
  let component: ProductElementComponent;
  let fixture: ComponentFixture<ProductElementComponent>;
  let debugElement: DebugElement;
  let service: FavouritesService;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [
        ProductElementComponent,
        CurrencyPipe,
        {provide: FavouritesService},
      ]
    });

    fixture = TestBed.createComponent(ProductElementComponent);
    debugElement = fixture.debugElement;
    service = TestBed.inject(FavouritesService);
    registerLocaleData(localeEs);
  }));

  /*it('should create',
    inject([FavouritesService], (injectService: FavouritesService) => {
      const data: ProductInterface = {
        title: 'Test item',
        description: 'Test description',
        price: 999,
        email: 'testmail@test.com',
        image: 'https://webpublic.s3-eu-west-1.amazonaws.com/tech-test/img/iphone.png'
      };
      component = new ProductElementComponent(injectService);
      component.product = data;

      component.ngOnInit();

      expect(component).toBeTruthy();
    }));*/

  /*it('should call isFavourite method on init', () => {
    // set up spies, could also call a fake method in case you don't want the API call to go through
    const userServiceSpy = spyOn(service, 'isFavourite').and.callThrough();

    // make sure they haven't been called yet
    expect(userServiceSpy).not.toHaveBeenCalled();

    // depending on how your component is set up, fixture.detectChanges() might be enough
    fixture.detectChanges()
    //component.ngOnInit();

    expect(userServiceSpy).toHaveBeenCalledTimes(1);
  });*/
});
