import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';

@Component({
  selector: 'app-search-bar',
  templateUrl: './search-bar.component.html',
  styleUrls: ['./search-bar.component.scss']
})
export class SearchBarComponent implements OnInit {
  @Output() filter: EventEmitter<string> = new EventEmitter<string>();
  @Output() sort: EventEmitter<string> = new EventEmitter<string>();
  @Input() isFavModal: boolean;

  searchTerm: string;
  sortBy: string = 'title';

  constructor() { }

  ngOnInit(): void {
  }

  searchItems(): void {
    this.filter.emit(this.searchTerm);
  }

  sortProductList(): void {
    this.sort.emit(this.sortBy);
  }
}
