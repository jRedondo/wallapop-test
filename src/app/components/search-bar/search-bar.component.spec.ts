import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {SearchBarComponent} from './search-bar.component';
import {By} from '@angular/platform-browser';
import {Test} from 'tslint';

describe('Component: SearchBar', () => {
  let component: SearchBarComponent;
  let fixture: ComponentFixture<SearchBarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [SearchBarComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SearchBarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should emit filter onclick', () => {
    const fixture = TestBed.createComponent(SearchBarComponent);
    const component = fixture.componentInstance;

    component.searchTerm = 'Camera';
    spyOn(component.filter, 'emit');

    const nativeElement = fixture.nativeElement;
    const button = nativeElement.querySelector('.search-button');
    button.dispatchEvent(new Event('click'));

    fixture.detectChanges();

    expect(component.filter.emit).toHaveBeenCalledWith('Camera');
  });

  it('should emit sort onclick', () => {
    const component = fixture.componentInstance;
    fixture.detectChanges();
    const select: HTMLSelectElement = fixture.debugElement.query(By.css('select')).nativeElement;

    spyOn(component.sort, 'emit');

    select.value = select.options[1].value;
    select.dispatchEvent(new Event('change'));

    fixture.detectChanges();

    expect(component.sort.emit).toHaveBeenCalledWith('title');
  });

  it('should hide sort if it\'s favourite modal', () => {
    const fixture = TestBed.createComponent(SearchBarComponent);
    fixture.whenStable().then(() => {
      const component = fixture.componentInstance;
      component.isFavModal = true;

      expect(fixture.debugElement.query(By.css('.sort-container')).nativeElement).toBeFalsy();
    });
  });

  it('should show sort if it\'s not modal', () => {
    const component = fixture.componentInstance;
    component.isFavModal = false;

    expect(fixture.debugElement.query(By.css('.sort-container')).nativeElement).toBeTruthy();
  });
});
