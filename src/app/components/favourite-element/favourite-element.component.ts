import { Component, OnInit, Input } from '@angular/core';
import {ProductInterface} from '../../models/product';
import { FavouritesService } from '../../services/favourites.service';

@Component({
  selector: 'app-favourite-element',
  templateUrl: './favourite-element.component.html',
  styleUrls: ['./favourite-element.component.scss']
})
export class FavouriteElementComponent implements OnInit {
  @Input() product: ProductInterface;
  isFavourite: boolean;

  onClickAddRemoveFavourites(product) {
    if (this.isFavourite) {
      this.favouriteService.removeFromFavourites(product);
      this.isFavourite = false;
    } else {
      this.favouriteService.addToFavourites(product);
      this.isFavourite = true;
    }
  }

  constructor(
    private favouriteService: FavouritesService
  ) { }

  ngOnInit(): void {
    this.isFavourite = true;
  }

}
