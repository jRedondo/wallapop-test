import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {HttpClientModule} from '@angular/common/http';
import {RouterModule} from '@angular/router';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';

import {AppComponent} from './app.component';
import {ProductListComponent} from './components/product-list/product-list.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {HttpClientInMemoryWebApiModule} from 'angular-in-memory-web-api';
import {InMemoryDataService} from './in-memory-data.service';
import {SearchBarComponent} from './components/search-bar/search-bar.component';
import {HeaderComponent} from './components/header/header.component';
import {ProductElementComponent} from './components/product-element/product-element.component';
import {SortByPipe} from './pipe/sortBy.pipe';
import {PaginationComponent} from './components/app-pagination/app-pagination.component';
import {ModalModule} from './components/_modal';
import { FavouriteElementComponent } from './components/favourite-element/favourite-element.component';
import { FavouriteListComponent } from './components/favourite-list/favourite-list.component';
import { FavouriteModalComponent } from './components/favourite-modal/favourite-modal.component';

@NgModule({
  imports: [
    BrowserModule,
    HttpClientModule,
    HttpClientInMemoryWebApiModule.forRoot(InMemoryDataService),
    ReactiveFormsModule,
    BrowserAnimationsModule,
    RouterModule.forRoot([
      {path: '', component: ProductListComponent},
    ]),
    FormsModule,
    ModalModule
  ],
  declarations: [
    AppComponent,
    ProductListComponent,
    FavouriteListComponent,
    SearchBarComponent,
    HeaderComponent,
    ProductElementComponent,
    PaginationComponent,
    FavouriteElementComponent,
    FavouriteModalComponent
  ],
  bootstrap: [
    AppComponent
  ]
})
export class AppModule {
}


/*
Copyright Google LLC. All Rights Reserved.
Use of this source code is governed by an MIT-style license that
can be found in the LICENSE file at http://angular.io/license
*/
