export interface ProductInterface {
  title: string;
  description: string;
  price: number;
  email: string;
  image: string;
}
